package com.gub;

import javax.persistence.EntityManager;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {
        Address address = new Address();
        address.setCity("Dhaka")
                .setCountry("Bangladesh")
                .setPostcode("1000")
                .setStreet("Poribagh");
        EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
        em.getTransaction()
                .begin();
        em.persist(address);
        em.getTransaction()
                .commit();
        em.close();
        PersistenceManager.INSTANCE.close();
    }
}
